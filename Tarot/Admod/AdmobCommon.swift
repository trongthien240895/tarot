//
//  AdmobCommon.swift
//  contact-manager
//
//  Created by vu trong thien on 8/4/18.
//  Copyright © 2018 vu trong thien. All rights reserved.
//

import Foundation
import GoogleMobileAds
import Reachability


class AdMobCommon: NSObject {

    static let shared = AdMobCommon()
    var interstitial: GADInterstitial?
    let requestVideoReward = GADRequest()

    override private init() {
    }

    func loadInterstitialAds(_ controller: UIViewController) {
        interstitial = GADInterstitial(adUnitID: AdMobKey.interstitalKey.rawValue)
        interstitial?.delegate = controller as? GADInterstitialDelegate
        let request = GADRequest()
        request.testDevices = [kGADSimulatorID, "4908278bf8093104209fecbb1b351cbc"]
        interstitial?.load(request)
    }

    func loadVideoReward(_ videoReward: GADRewardBasedVideoAd) {
        let reachability =  Reachability()
        reachability?.whenReachable = { reachability in
            self.requestVideoReward.testDevices = [kGADSimulatorID, "4908278bf8093104209fecbb1b351cbc"]
            videoReward.load(self.requestVideoReward, withAdUnitID: AdMobKey.videoKey.rawValue)
        }
        do {
            try reachability?.startNotifier()
        } catch {
            // TODO
        }
    }

}

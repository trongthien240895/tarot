//
//  JsonKey.swift
//  Tarot
//
//  Created by VuTrongThien on 9/4/18.
//  Copyright © 2018 VuTrongThien. All rights reserved.
//

import Foundation

enum JsonKey: String {
    case cards = "cards"
    case majorArcana = "MajorArcana"
    case cups = "Cups"
    case swords = "Swords"
    case wands = "Wands"
    case pentacles = "Pentacles"
    case name = "name"
    case title = "title"
}

//
//  AppCommon.swift
//  Tarot
//
//  Created by VuTrongThien on 9/4/18.
//  Copyright © 2018 VuTrongThien. All rights reserved.
//

import Foundation
import FirebaseStorage

enum CardCategory: String {
    case majorArcana = "MajorArcana"
    case cups = "Cups"
    case pentacles = "Pentacles"
    case swords = "Swords"
    case wands = "Wands"
}

enum TarotFolder: String {
    case card = "TarotCard"
    case mean = "TarotCardMean"
    case card2 = "TarotCard2"
}

enum TarotExtention: String {
    case png = "png"
    case txt = "txt"
}

enum TarotType: String {
    case work = "Work&Career"
    case success = "Success"
    case travel = "Travel"
    case marriage = "Marriage"
    case money = "Money"
    case life = "Life"
    case health = "Health"
    case love = "Love"
    case family = "Family"
}

enum AdMobKey: String {
    case appKeyTest = "ca-app-pub-3940256099942544~1458002511"
    case interstitalKeyTest = "ca-app-pub-3940256099942544/4411468910"
    case bannerKeyTest = "ca-app-pub-3940256099942544/2934735716"
    case videoKeyTest = "ca-app-pub-3940256099942544/1712485313"

    case appKey = "ca-app-pub-8165985474472522~1785733239"
    case interstitalKey = "ca-app-pub-8165985474472522/1284237649"
    case bannerKey = "ca-app-pub-8165985474472522/2161686494"
    case videoKey = "ca-app-pub-8165985474472522/2464938823"
}

let horoscopes = ["Aries","Taurus","Gemini","Cancer","Leo","Virgo","Libra",
                  "Scorpio","Sagittarius","Capricorn","Aquarius","Pisces"]

class AppCommon: NSObject {
    
    let storage = Storage.storage()
    let storageRef: StorageReference?
    private override init() {
        storageRef = storage.reference()
    }
    
    class var shareInstance: AppCommon {
        struct Instance {
            static let instance = AppCommon()
        }
        return Instance.instance
    }
    
    func downloadFromFirebase(folder: String, name: String, extention: String) {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let localURL = documentsURL.appendingPathComponent("\(folder)/\(name).\(extention)")
        let islandRef = storageRef?.child("\(folder)/\(name).\(extention)")
        if FileManager.default.fileExists(atPath: localURL.path) {
            // TODO
            return
        }
        islandRef?.write(toFile: localURL) { url, error in
            if error != nil{
                print("error === \(name)")
                // TODO
            } else {
                print("success")
                // TODO
            }
        }
    }

    func getRandomNumber(range: Int?, count: Int?) -> [Int]? {
        guard let range = range, let count = count else {
            return nil
        }
        var i = 0
        var numbers: [Int] = []
        while i < count {
            var test = 0
            let randomCount = UInt32(range - 1)
            let randomNum = Int(arc4random_uniform(randomCount))
            numbers.forEach { (number) in
                if number == randomNum {
                    test = test + 1
                }
            }
            if test == 0 {
                i = i + 1
                numbers.append(randomNum)
            }

        }
        return numbers
    }
    
}

func getCurrentDate(format: String = "dd.MM.yyyy") -> String {
    let date = Date()
    let formatter = DateFormatter()
    formatter.dateFormat = format
    let result = formatter.string(from: date)
    return result
}


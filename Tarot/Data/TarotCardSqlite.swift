//
//  TarotCardManager+Sqlite.swift
//  Tarot
//
//  Created by vu trong thien on 9/11/18.
//  Copyright © 2018 VuTrongThien. All rights reserved.
//

import Foundation
import SQLite

class TarotCardSqlite: NSObject {

    /*
     Database name and extension
     */
    private let names = "names"
    private let dbExtensionName = "db"
    private let horoscope = "horoscope"

    override init() {
        super.init()
        copyDatabase()
    }

    class var shareInstance: TarotCardSqlite  {
        struct Instance {
            static let instance = TarotCardSqlite()
        }
        return Instance.instance
    }

    //    copy database

    private func copyDatabase() {
        let documentsPath = NSURL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])

        // Copy names database
        if let destinationSqliteURL = documentsPath.appendingPathComponent("\(names).db")  {
            if !FileManager.default.fileExists(atPath: destinationSqliteURL.path) {
                if let sourcePath = Bundle.main.url(forResource: names, withExtension: "db") {
                    do {
                        try FileManager.default.copyItem(at: sourcePath, to: destinationSqliteURL)
                        print("\(#function) \(destinationSqliteURL)")
                    } catch {
                        //TODO
                    }
                }
            }
        }

        // Copy horoscope database
        if let destinationSqliteURL = documentsPath.appendingPathComponent("\(horoscope).db")  {
            if !FileManager.default.fileExists(atPath: destinationSqliteURL.path) {
                if let sourcePath = Bundle.main.url(forResource: horoscope, withExtension: "db") {
                    do {
                        try FileManager.default.copyItem(at: sourcePath, to: destinationSqliteURL)
                        print("\(#function) \(destinationSqliteURL)")
                    } catch {
                        //TODO
                    }
                }
            }
        }
    }

    func connectData() throws -> Connection {
        let documentsPath = NSURL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        let database = try Connection("\(documentsPath)/\(names).db")
        return database
    }

    func connectHorosopeData() throws -> Connection {
        let documentsPath = NSURL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        let database = try Connection("\(documentsPath)/\(horoscope).db")
        return database
    }

}

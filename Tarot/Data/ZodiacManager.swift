//
//  ZodiacManager.swift
//  Tarot
//
//  Created by vu trong thien on 9/26/18.
//  Copyright © 2018 VuTrongThien. All rights reserved.
//

import Foundation

class ZodiacManager: NSObject {

    func getZodiacCompatJson(name: String, parner: String) -> String? {
        do {
            let path = Bundle.main.path(forResource: "compat_details", ofType: "json")
            let data = try Data(contentsOf: URL(fileURLWithPath: path!), options: .mappedIfSafe)
            let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
            guard let results = jsonResult as? [[String: AnyObject]] else {
                return nil
            }
            for result in results {
                guard let zodiacName = result["sign"] as? String,
                    let datas = result["data"] as? [[String: AnyObject]] else {
                        return nil
                }
                if zodiacName == name {
                    return getCompatString(datas: datas, name: parner)
                } else if zodiacName == parner {
                    return getCompatString(datas: datas, name: name)
                }
            }
            return nil
        } catch {
            return nil
        }
    }

    func getCompatString(datas: [[String: AnyObject]], name: String) -> String? {
        var result: String?
        datas.forEach({ (data) in
            if let zodiacParner = data["name"] as? String, zodiacParner == name {
                result = data["value"] as? String
                return
            }
        })
        return result
    }

}

//
//  TarotCardManager.swift
//  Tarot
//
//  Created by VuTrongThien on 9/4/18.
//  Copyright © 2018 VuTrongThien. All rights reserved.
//

import Foundation
import SwiftyJSON

class TarotCardManager: NSObject {
    
    func getCardJson(category: CardCategory) -> [TarotCard]? {
        do {
            let path = Bundle.main.path(forResource: "cards", ofType: "json")
            let data = try Data(contentsOf: URL(fileURLWithPath: path!), options: .mappedIfSafe)
            let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
            guard let result = jsonResult as? [String: AnyObject],
                let cardData = result["Cards"] as? [String: AnyObject],
                let cardJsons = cardData[category.rawValue] as? [[String: AnyObject]] else {
                    return nil
                }
            var tarotCards = [TarotCard]()
            cardJsons.forEach { (cardJson) in
                tarotCards.append(TarotCard(json: JSON(cardJson)))
            }
            return tarotCards
        } catch {
            return nil
        }
    }
    
    func getCardMeanJson(category: CardCategory) {
        do {
            let path = Bundle.main.path(forResource: "cards_mean", ofType: "json")
            let data = try Data(contentsOf: URL(fileURLWithPath: path!), options: .mappedIfSafe)
            let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
            guard let result = jsonResult as? [String: AnyObject],
                let cardData = result["Cards"] as? [String: AnyObject],
                let cardJsons = cardData[category.rawValue] as? [[String: AnyObject]] else {
                    return
            }
            var tarotCards = [TarotCard]()
            cardJsons.forEach { (cardJson) in
                tarotCards.append(TarotCard(json: JSON(cardJson)))
            }
        } catch {
        }
    }
    
    func getCard2Json() -> [TarotCard]? {
        do {
            let path = Bundle.main.path(forResource: "cards2", ofType: "json")
            let data = try Data(contentsOf: URL(fileURLWithPath: path!), options: .mappedIfSafe)
            let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
            guard let result = jsonResult as? [String: AnyObject],
                let cardData = result["Cards"] as? [String: AnyObject],
                let cardJsons = cardData["Data"] as? [[String: AnyObject]] else {
                    return nil
            }
            var tarotCards = [TarotCard]()
            cardJsons.forEach { (cardJson) in
                tarotCards.append(TarotCard(json: JSON(cardJson)))
            }
            return tarotCards
        } catch {
            return nil
        }
    }
    
}

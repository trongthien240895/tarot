//
//  ViewController.swift
//  Tarot
//
//  Created by VuTrongThien on 9/4/18.
//  Copyright © 2018 VuTrongThien. All rights reserved.
//

import UIKit
import SwiftyJSON

class ViewController: UIViewController {
    
    var tarotCards: [TarotCard]?
    var tarotCardManager: TarotCardManager?

    @IBOutlet weak var tarotCardImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tarotCardManager = TarotCardManager()
        tarotCards = tarotCardManager?.getCardJson(category: .majorArcana)
        tarotCards?.forEach({ (card) in
            if let name = card.name, name.contains("_0.png") {
                AppCommon.shareInstance.downloadFromFirebase(folder: "TarotCard", name: name, extention: "png")
                print(name)
            }
        })
        guard let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else {
            return
        }
        let filePath = documentsURL.appendingPathComponent("TarotCard/0_TheFool_0.png")
        if let data = try? Data(contentsOf: filePath) {
            tarotCardImage.image = UIImage(data: data)
        }
    }
    
}


//
//  HoroscopeData.swift
//  Tarot
//
//  Created by vu trong thien on 9/21/18.
//  Copyright © 2018 VuTrongThien. All rights reserved.
//

import Foundation
import SQLite

class HoroscopeData: NSObject {

    let horosopeTable = Table(HoroscopeKey.tableName.rawValue)
    let date = Expression<String?>(HoroscopeKey.date.rawValue)
    let horoscope = Expression<String?>(HoroscopeKey.horoscope.rawValue)
    let terrot = Expression<String?>(HoroscopeKey.terrot.rawValue)
    let sunsign = Expression<String?>(HoroscopeKey.sunsign.rawValue)

    var database: Connection?

    override init() {
        do {
            database = try TarotCardSqlite.shareInstance.connectHorosopeData()
        } catch {
            // TODO
        }
    }

    func getHoroscope(date: String, sunsign: String) -> Horoscope? {
        guard let database = database else {
            return nil
        }
        let horoscope = Horoscope()
        let horosopeByName = horosopeTable.filter(sunsign == self.sunsign).filter(date == self.date)
        do {
            let horoscopeArr = try database.prepare(horosopeByName)
            for row in horoscopeArr {
                horoscope.date = row[self.date]
                horoscope.horoscopeDes = row[self.horoscope]
                horoscope.terrot = row[terrot]
                horoscope.sunsign = row[self.sunsign]
            }
            return horoscope
        } catch  {
            return nil
        }
    }
    
    func getDate(name: String) -> [String]? {
        guard let database = database else {
            return nil
        }
        var dates: [String] = []
        let dateByName = horosopeTable.filter(name == sunsign)
        do {
            let dateArr = try database.prepare(dateByName)
            for row in dateArr {
                if let time = row[date] {
                    dates.append(time)
                }
            }
            return dates
        } catch  {
            return nil
        }
    }
}

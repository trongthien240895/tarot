//
//  TarotCardData.swift
//  Tarot
//
//  Created by vu trong thien on 9/11/18.
//  Copyright © 2018 VuTrongThien. All rights reserved.
//

import Foundation
import SQLite

class TarotCardData: NSObject {

    let tarotCards = Table(TarotCardKey.tableName.rawValue)
    let id = Expression<Int?>("id")
    let cardName = Expression<String?>(TarotCardKey.cardName.rawValue)
    let cardImage = Expression<String?>(TarotCardKey.cardImage.rawValue)
    let cardDescription = Expression<String?>(TarotCardKey.description.rawValue)
    let love = Expression<String?>(TarotCardKey.love.rawValue)
    let work = Expression<String?>(TarotCardKey.work.rawValue)
    let finance = Expression<String?>(TarotCardKey.finance.rawValue)
    let yesNo = Expression<String?>(TarotCardKey.yesNo.rawValue)
    let health = Expression<String?>(TarotCardKey.health.rawValue)

    var database: Connection?

    override init() {
        do {
            database = try TarotCardSqlite.shareInstance.connectData()
        } catch {
            // TODO
        }
    }

    func getTarotCard(imageName: String?) -> TarotCard? {
        guard let database = database else {
            return nil
        }
        let tarot = TarotCard()
        let tarotByImage = tarotCards.filter(imageName == cardImage)
        do {
            let tarotArr = try database.prepare(tarotByImage)
            for row in tarotArr {
                tarot.cardName = row[cardName]
                tarot.image = row[cardImage]
                tarot.cardDescription = row[cardDescription]
                tarot.love = row[love]
                tarot.work = row[work]
                tarot.finance = row[finance]
                tarot.yesNo = row[yesNo]
                tarot.health = row[health]
                tarot.id = row[id]
            }
            return tarot
        } catch  {
            return nil
        }
    }

    func getAll() -> [TarotCard]? {
        guard let database = database else {
            return nil
        }
        var arr = [TarotCard]()
        do {
            let tarotArr = try database.prepare(tarotCards)
            for row in tarotArr {
                let tarot = TarotCard()
                tarot.cardName = row[cardName]
                tarot.image = row[cardImage]
                tarot.cardDescription = row[cardDescription]
                tarot.love = row[love]
                tarot.work = row[work]
                tarot.finance = row[finance]
                tarot.yesNo = row[yesNo]
                tarot.health = row[health]
                tarot.id = row[id]
                arr.append(tarot)
            }
            return arr
        } catch  {
            return nil
        }
    }

    func updateCard(cardid: Int, image: String) {
        let card = tarotCards.filter(id == cardid)
        do {
            try database?.run(card.update(cardImage <- image))
        } catch  {

        }
    }

}

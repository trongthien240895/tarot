//
//  TarotCard.swift
//  Tarot
//
//  Created by VuTrongThien on 9/4/18.
//  Copyright © 2018 VuTrongThien. All rights reserved.
//

import Foundation
import SwiftyJSON

class TarotCard: NSObject {
    
    var id: Int?
    var name: String?
    var title: String?
    var image: String?
    var cardName: String?
    var cardDescription: String?
    var love: String?
    var work: String?
    var finance: String?
    var yesNo: String?
    var health: String?
    
    convenience init(json: JSON) {
        self.init()
        name = json[JsonKey.name.rawValue].string
        title = json[JsonKey.title.rawValue].string
    }
    
}

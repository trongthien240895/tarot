//
//  TarotCardKey.swift
//  Tarot
//
//  Created by vu trong thien on 9/11/18.
//  Copyright © 2018 VuTrongThien. All rights reserved.
//

import Foundation

enum TarotCardKey: String {

    case tableName = "message"
    case cardName = "card_name"
    case cardImage = "card_image"
    case description = "card_description"
    case love = "love_description"
    case work = "work_description"
    case finance = "finance_description"
    case yesNo = "yes_no_description"
    case health = "health_description"

}

enum HoroscopeKey: String {

    case tableName = "zodiac"
    case date = "Date"
    case horoscope = "Horoscope"
    case terrot = "Terrot"
    case sunsign = "Sunsign"
}

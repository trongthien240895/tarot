//
//  ZodiacCompatViewController.swift
//  Tarot
//
//  Created by vu trong thien on 9/26/18.
//  Copyright © 2018 VuTrongThien. All rights reserved.
//

import UIKit

class ZodiacCompatViewController: UIViewController {

    @IBOutlet weak var zodiacParnerView: UIView!
    @IBOutlet weak var zodiacView: UIView!
    @IBOutlet weak var zodiacColection: UICollectionView!
    @IBOutlet weak var zodiacImage: UIImageView!
    @IBOutlet weak var zodiacParnerImage: UIImageView!
    
    var zodiacData: ZodiacManager?
    var test = 0
    var names = ["", ""]

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigationBar()
        zodiacData = ZodiacManager()
        addGesture()
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        clearData()
    }

    func setUpView() {
        zodiacView.setOvalView()
        zodiacParnerView.setOvalView()
    }

    func setUpNavigationBar() {
        navigationController?.clearColor()
        navigationItem.addImageNavigation(name: "logo-zodiac", color: .white)
    }
    
    func clearData() {
        zodiacParnerImage.image = nil
//        zodiacImage.image = nil
        names[1] = ""
    }
    
    func animationImage() {
        let zodiacFrame = zodiacView.frame
        let parnerFrame = zodiacParnerView.frame
        
        // zodiacImage animation
        UIView.animate(withDuration: 1, animations: {
            self.zodiacView.frame = parnerFrame
        }) { (finish: Bool) in
            UIView.animate(withDuration: 0.5, animations: {
                self.zodiacView.frame = zodiacFrame
            })
        }
        
        // zodiacparnerImage animation
        UIView.animate(withDuration: 1, animations: {
            self.zodiacParnerView.frame = zodiacFrame
        }) { (finish: Bool) in
            UIView.animate(withDuration: 0.5, animations: {
                self.zodiacParnerView.frame = parnerFrame
            })
        }
    }
    
    func pushDetailController() {
        guard let controller = storyboard?.instantiateViewController(withIdentifier: "ZodiacCompatDetailViewController") as? ZodiacCompatDetailViewController,
         let detail = zodiacData?.getZodiacCompatJson(name: names[0], parner: names[1]) else {
            return
        }
        controller.compatDetail = detail
        controller.names = names
        navigationController?.pushViewController(controller, animated: false)
    }
    
    func addGesture() {
        let zodiacGesture = UITapGestureRecognizer(target: self, action: #selector(zodiacImageTap))
        zodiacImage.addGestureRecognizer(zodiacGesture)
        let zodiacParnerGesture = UITapGestureRecognizer(target: self, action: #selector(zodiacParnerImageTap))
        zodiacParnerImage.addGestureRecognizer(zodiacParnerGesture)
    }
    
    @objc func zodiacImageTap() {
        zodiacImage.image = nil
        names[0] = ""
    }
    
    @objc func zodiacParnerImageTap() {
        zodiacParnerImage.image = nil
        names[1] = ""
    }

}

extension ZodiacCompatViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return horoscopes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ZodiacCollectionCell", for: indexPath) as? ZodiacCollectionCell else {
            return UICollectionViewCell()
        }
        cell.bindData(name: horoscopes[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 3, height: 90)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if names[0] == "" {
            names[0] = horoscopes[indexPath.row]
            zodiacImage.image = UIImage(named: "\(names[0].lowercased())1")
            zodiacImage.scaleImage()
        } else if names[1] == "" {
            names[1] = horoscopes[indexPath.row]
            zodiacParnerImage.image = UIImage(named: "\(names[1].lowercased())1")
            zodiacParnerImage.scaleImage()
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.animationImage()
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.4) {
                self.pushDetailController()
            }
        }
    }
    
}

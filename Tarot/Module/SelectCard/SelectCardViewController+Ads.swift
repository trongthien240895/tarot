//
//  SelectCardViewController+Ads.swift
//  Tarot
//
//  Created by vu trong thien on 9/12/18.
//  Copyright © 2018 VuTrongThien. All rights reserved.
//

import Foundation
import UIKit
import GoogleMobileAds

extension SelectCardViewController: GADRewardBasedVideoAdDelegate, GADInterstitialDelegate {

    // Admob: Video reward

    func rewardBasedVideoAd(_ rewardBasedVideoAd: GADRewardBasedVideoAd,
                            didRewardUserWith reward: GADAdReward) {
        UserDefaults.standard.set(nil, forKey: dateKey)
        UserDefaults.standard.set(nil, forKey: tarotKey)
        selectedState = .none
        print("Reward received with currency: \(reward.type), amount \(reward.amount).")
    }

    func rewardBasedVideoAdDidReceive(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        print("Reward based video ad is received.")
    }

    func rewardBasedVideoAdDidClose(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        print("Reward based video ad is closed.")
        AdMobCommon.shared.loadVideoReward(videoReward)
    }

    func rewardBasedVideoAd(_ rewardBasedVideoAd: GADRewardBasedVideoAd,
                            didFailToLoadWithError error: Error) {
        print("Reward based video ad failed to load. \(error)")
    }

    // Admob: Full screen
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        if ad.isReady {
            ad.present(fromRootViewController: self)
        }
    }

    func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
        print("AdMod \(error)")
    }

}

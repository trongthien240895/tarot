//
//  SelectCardViewController.swift
//  Tarot
//
//  Created by VuTrongThien on 9/5/18.
//  Copyright © 2018 VuTrongThien. All rights reserved.
//

import UIKit
import GoogleMobileAds

enum SelectedState {
    case none
    case isRandom
    case finish
}

class SelectCardViewController: UIViewController {

    @IBOutlet weak var selectedCardImage: UIImageView!
    @IBOutlet weak var magicEyeImage: UIImageView!
    @IBOutlet weak var crystalsImage: UIImageView!
    

    var tarotCards: [TarotCard]?
    var isSelected = false
    var tarotCardManager: TarotCardManager?
    var timer: Timer?
    var currentTarotCard: TarotCard?
    var selectedState = SelectedState.none
    var tarotCardData: TarotCardData?
    var dateKey = "DateKey"
    var tarotKey = "TarotKey"
    var videoReward = GADRewardBasedVideoAd()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tarotCards = []
        tarotCardManager = TarotCardManager()
        getAllTarotCard()
        addGesture()
        setUpNavigationBar()
        tarotCardData = TarotCardData()
        videoReward.delegate = self
        AdMobCommon.shared.loadInterstitialAds(self)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.crystalsImage.scaleImage()
        }
        AdMobCommon.shared.loadVideoReward(videoReward)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpView()
        navigationController?.navigationBar.topItem?.title = ""
    }

    func setUpNavigationBar() {
        navigationController?.clearColor()
        navigationItem.addImageNavigation(name: "title_logo", color: .white)
    }

    func setUpView() {
        if !checkCurrentDate() {
            crystalsImage.isHidden = true
            selectedCardImage.image = UIImage(named: "backcard5")
            selectedCardImage.isUserInteractionEnabled = false
            return
        }
        guard let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first,
            let title = UserDefaults.standard.string(forKey: tarotKey) else {
            return
        }
        selectedCardImage.isUserInteractionEnabled = true
        crystalsImage.isHidden = false
        let filePath = documentsURL.appendingPathComponent("\(TarotFolder.card2.rawValue)/\(title.uppercased()).png")
        if let data = try? Data(contentsOf: filePath) {
            selectedCardImage.image = UIImage(data: data)
        }
    }

    func checkCurrentDate() -> Bool {
        if getCurrentDate() == UserDefaults.standard.string(forKey: dateKey) {
            return true
        }
        return false
    }
    
    func getAllTarotCard() {
        tarotCards = tarotCardManager?.getCard2Json()
        tarotCards?.forEach({ (card) in
            if let title = card.title  {
                AppCommon.shareInstance.downloadFromFirebase(folder: TarotFolder.card2.rawValue,
                                                             name: title.uppercased(),
                                                             extention: TarotExtention.png.rawValue)
            }
        })
    }
    
    func gotoDetailViewController() {
        if checkCurrentDate() {
            let storyboard = UIStoryboard(name: "TarotCard", bundle: nil)
            if let detailController = storyboard.instantiateViewController(withIdentifier: "TarotDetailViewController") as? TarotDetailViewController,
                let imageName = UserDefaults.standard.string(forKey: tarotKey) {
                detailController.tarotCard = tarotCardData?.getTarotCard(imageName: imageName.uppercased())
                navigationController?.pushViewController(detailController, animated: true)
            }
            return
        }
    }
    
    func addGesture() {
        let eyeGesture = UITapGestureRecognizer(target: self, action: #selector(magicEyeTap))
        magicEyeImage.addGestureRecognizer(eyeGesture)
        let cardGesture = UITapGestureRecognizer(target: self, action: #selector(cardTap))
        selectedCardImage.addGestureRecognizer(cardGesture)
        let crystalsGesture = UITapGestureRecognizer(target: self, action: #selector(crystalsTap))
        crystalsImage.addGestureRecognizer(crystalsGesture)
    }

}

//
//  SelectCardViewController+UiTapped.swift
//  Tarot
//
//  Created by vu trong thien on 9/13/18.
//  Copyright © 2018 VuTrongThien. All rights reserved.
//

import Foundation
import UIKit

extension SelectCardViewController {

    @objc func updateImage() {
        guard let count = tarotCards?.count, count > 0 else {
            return
        }
        let randomCount = UInt32(count - 1)
        let randomNum = Int(arc4random_uniform(randomCount))
        guard let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first,
            let title = tarotCards?[randomNum].title else {
                return
        }
        let filePath = documentsURL.appendingPathComponent("\(TarotFolder.card2.rawValue)/\(title.uppercased()).png")
        if let data = try? Data(contentsOf: filePath) {
            selectedCardImage.image = UIImage(data: data)
            currentTarotCard = tarotCards?[randomNum]
        }
    }

    @objc func cardTap(sender: UITapGestureRecognizer) {
        if checkCurrentDate() {
            let storyboard = UIStoryboard(name: "TarotCard", bundle: nil)
            if let detailController = storyboard.instantiateViewController(withIdentifier: "TarotDetailViewController") as? TarotDetailViewController,
                let imageName = UserDefaults.standard.string(forKey: tarotKey) {
                detailController.tarotCard = tarotCardData?.getTarotCard(imageName: imageName.uppercased())
                navigationController?.pushViewController(detailController, animated: true)
            }
            return
        }
    }

    @objc func magicEyeTap(sender: UITapGestureRecognizer) {
        if checkCurrentDate() {
            gotoDetailViewController()
            return
        }
        switch selectedState {
        case .none:
            magicEyeImage.rotateImage()
            selectedState = .isRandom
            timer = Timer.scheduledTimer(timeInterval: 0.02, target: self,
                                         selector: #selector(updateImage), userInfo: nil, repeats: true)
        case .isRandom:
            selectedState = .finish
            timer?.invalidate()
            timer = nil
            UserDefaults.standard.set(getCurrentDate(), forKey: dateKey)
            UserDefaults.standard.set(currentTarotCard?.title, forKey: tarotKey)
            selectedCardImage.isUserInteractionEnabled = true
            UIView.animate(withDuration: 0.5, animations: {
                self.crystalsImage.isHidden = false
            }) { (_: Bool) in
                self.crystalsImage.scaleImage()
            }
        case .finish:
           gotoDetailViewController()
        }

    }

    @objc func crystalsTap(sender: UITapGestureRecognizer) {
        let alert = UIAlertController(title: "Tarot",
                                      message: "One more time",
                                      preferredStyle: .alert)
        // Submit button
        let submitAction = UIAlertAction(title: "Get free",
                                         style: .default,
                                         handler: {(action) -> Void in
                                            if self.videoReward.isReady {
                                                self.videoReward.present(fromRootViewController: self)
                                            } else {
                                                UIAlertView(title: "Video reward", message: "Can not load video \nPlease check internet",
                                                            delegate: nil, cancelButtonTitle: "OK").show()
                                            }
        })
        let cancel = UIAlertAction(title: "Hủy",
                                   style: .destructive,
                                   handler: {(action) -> Void in
        })

        alert.addAction(submitAction)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)

    }

}

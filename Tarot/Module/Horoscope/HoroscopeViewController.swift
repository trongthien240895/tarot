//
//  HoroscopeViewController.swift
//  Tarot
//
//  Created by vu trong thien on 9/21/18.
//  Copyright © 2018 VuTrongThien. All rights reserved.
//

import UIKit

class HoroscopeViewController: UIViewController {

    @IBOutlet weak var horoscopeCollection: UICollectionView!

    var horoscopeData: HoroscopeData?

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigationBar()
        getAllTarotCard()
        horoscopeData = HoroscopeData()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tabBarController?.tabBar.clearColor()
    }

    func setUpNavigationBar() {
        navigationController?.clearColor()
        navigationItem.addImageNavigation(name: "logo-zodiac", color: .white)
    }

    func getAllTarotCard() {
        let tarotCardManager = TarotCardManager()
        let tarotCards = tarotCardManager.getCard2Json()
        tarotCards?.forEach({ (card) in
            if let title = card.title  {
                AppCommon.shareInstance.downloadFromFirebase(folder: TarotFolder.card2.rawValue,
                                                             name: title.uppercased(),
                                                             extention: TarotExtention.png.rawValue)
            }
        })
    }

}

extension HoroscopeViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return horoscopes.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HoroscopeCollectionViewCell",
                                                            for: indexPath) as? HoroscopeCell else {
            return UICollectionViewCell()
        }
        cell.bindData(name: horoscopes[indexPath.row])
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 3, height: 100)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let detailControler = storyboard?.instantiateViewController(withIdentifier: "HoroscopeDetailViewController")
            as? HoroscopeDetailViewController else {
            return
        }
        let horoscope = horoscopeData?.getHoroscope(date: getCurrentDate(format: "dd-MMM"), sunsign: horoscopes[indexPath.row])
        detailControler.horoscope = horoscope
        detailControler.currentIndex = indexPath.row
        navigationController?.pushViewController(detailControler, animated: true)
    }

}

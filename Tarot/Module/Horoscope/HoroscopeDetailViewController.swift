//
//  HoroscopeDetailViewController.swift
//  Tarot
//
//  Created by vu trong thien on 9/21/18.
//  Copyright © 2018 VuTrongThien. All rights reserved.
//

import UIKit

class HoroscopeDetailViewController: UIViewController {

    @IBOutlet weak var horoscopeImageContainer: UIView!
    @IBOutlet weak var horoscopeCollection: UICollectionView!
    @IBOutlet weak var horoscopeImage: UIImageView!
    @IBOutlet weak var horoscopeTextView: UITextView!
    @IBOutlet weak var datePicker: UIPickerView!
    
    var horoscope: Horoscope?
    var horoscopeData: HoroscopeData?
    var dates: [String]?
    var currentIndex = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        horoscopeData = HoroscopeData()
        fillData()
        setUpNavigationBar()
        horoscopeImageContainer.setOvalView()
        getDates()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        horoscopeCollection.scrollToItem(at: IndexPath(row: currentIndex, section: 0),
                                         at: .centeredHorizontally, animated: true)
    }

    private func fillData() {
        guard let image = horoscope?.sunsign, let terrot = horoscope?.terrot,
            let des = horoscope?.horoscopeDes else {
                return
        }
        navigationItem.title = horoscope?.sunsign
        horoscopeImage.image = UIImage(named: "\(image.lowercased())9")
        horoscopeTextView.text = "\(terrot) \n\n\(des)"
    }

    func getDates () {
        if let name = horoscope?.sunsign {
            dates = horoscopeData?.getDate(name: name)
        }
        if let index = getCurrentTimeIndex() {
            datePicker.selectRow(index, inComponent: 0, animated: false)
        }
    }
    
    func getCurrentTimeIndex() -> Int? {
        guard let count = dates?.count else {
            return nil
        }
        for i in 0...count - 1 {
            if dates?[i] == horoscope?.date {
                return i
            }
        }
        return nil
    }

    func setUpNavigationBar() {
        navigationController?.clearColor()
        navigationItem.addImageNavigation(name: "logo-zodiac", color: .white)
    }

}

extension HoroscopeDetailViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return horoscopes.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HoroscopeDetailCell",
                                                            for: indexPath) as? HoroscopeDetailCell else {
            return UICollectionViewCell()
        }
        cell.bindData(name: horoscopes[indexPath.row])
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let date = horoscope?.date {
            horoscope = horoscopeData?.getHoroscope(date: date, sunsign: horoscopes[indexPath.row])
        }
        navigationController?.navigationBar.topItem?.title = "haha"
        fillData()
    }

}

extension HoroscopeDetailViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dates?.count ?? 0
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        if let string = dates?[row] {
            return NSAttributedString(string: string, attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        } else {
            return nil
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if let date = dates?[row], let sunsign = horoscope?.sunsign {
            horoscope = horoscopeData?.getHoroscope(date: date, sunsign: sunsign)
        }
        fillData()
    }
    
}
